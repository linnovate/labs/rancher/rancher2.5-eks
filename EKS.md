```
sudo snap install aws-cli --classic
```

```
aws configure
```

```
curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
sudo mv /tmp/eksctl /usr/local/bin
```

```
eksctl create cluster \
 --name rancher-eks \
 --version 1.18 \
 --with-oidc \
 --without-nodegroup
 ```
