## Installing rancher with rancherd
```
curl -sfL https://get.rancher.io | sh -

systemctl enable rancherd-server.service

systemctl start rancherd-server.service
```
journalctl -eu rancherd-server -f

export KUBECONFIG=/etc/rancher/rke2/rke2.yaml PATH=$PATH:/var/lib/rancher/rke2/bin

rancherd reset-admin
